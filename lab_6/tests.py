from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import home
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_hello_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_hello_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_hello(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main.html')

    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_profile_home_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, home)

    def test_template_profile(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'HOME.html')

    def test_landing_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Hello, Apa kabar?',html_response)

    def test_create_object_model (self):
        status_message = Status.objects.create(mystatus = 'ok')
        counting_object_status = Status.objects.all().count()
        self.assertEqual(counting_object_status, 1)


class FunctionalVisitorTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable') 
        chrome_options.add_argument('--no-sandbox') 
        chrome_options.add_argument('--headless') 
        chrome_options.add_argument('disable-gpu') 
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalVisitorTest, self).setUp()


    def tearDown(self):
        self.browser.quit()
        super(FunctionalVisitorTest, self).tearDown()


    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://ppw-b-cacadn.herokuapp.com/')
        time.sleep(3) # Let the user actually see something!
        search_box = self.browser.find_element_by_name('mystatus')
        search_box.send_keys('Coba Coba')
        search_box.submit()
        time.sleep(5) # Let the user actually see something!
        self.assertIn( "Coba Coba", self.browser.page_source)

    def test_layout_profile(self):
        self.browser.get('http://ppw-b-cacadn.herokuapp.com/profile/')
        paragraph_text = self.browser.find_element_by_tag_name('p').text
        self.assertIn('SOON TO BE PROGRAMMER', paragraph_text)
        time.sleep(3)

    def test_header(self):
        self.browser.get('http://ppw-b-cacadn.herokuapp.com/')
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn("Hello, Apa kabar?", header_text)
        p_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn("What's On Your Mind?Post your status here!", p_text)
        time.sleep(5)
 
    def test_title(self):
        self.browser.get('http://ppw-b-cacadn.herokuapp.com/')
        time.sleep(5)
        self.assertIn("nathasyadn", self.browser.title)

    def test_header_with_css_property(self):
        self.browser.get('http://ppw-b-cacadn.herokuapp.com/')
        header_text = self.browser.find_element_by_tag_name('h1').value_of_css_property('text-align')
        time.sleep(5)
        self.assertIn('center', header_text)
    
    def test_response_header_with_css_property(self):
        self.browser.get('http://ppw-b-cacadn.herokuapp.com/')
        header_text = self.browser.find_element_by_tag_name('h1').value_of_css_property('font-size')
        time.sleep(5)
        self.assertIn('40px', header_text)